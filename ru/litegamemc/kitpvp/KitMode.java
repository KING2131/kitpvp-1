package ru.litegamemc.kitpvp;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.CauldronLevelChangeEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.*;
import ru.litegamemc.core.user.User;
import ru.litegamemc.core.user.UserUtils;
import ru.litegamemc.core.utils.*;
import ru.litegamemc.creative.menu.MyWorldMenu;
import ru.litegamemc.creative.realm.Realm;
import ru.litegamemc.kitpvp.kits.utils.KitMenu;
import ru.litegamemc.creative.menu.WorldsMenu;
import ru.litegamemc.creative.mode.utils.Mode;

public class KitMode implements Mode
{
  @Override
  public void loadPlayer(final Player player, final World world, final Realm realm)
  {
    player.setGameMode(GameMode.SPECTATOR);
    player.setAllowFlight(true);
    player.setFlying(true);
    player.sendTitle("", LocaleUtils.get(player).get("title.clickToContinue"), 0, 100, 5);
  }
  
  @Override
  public void loadWorld(final World world, final Realm realm)
  {
    world.setAutoSave(false);
    world.setPVP(true);
  }
  
  @Override
  public void onPlayerInteract(final PlayerInteractEvent event, final Realm realm)
  {
    final Player player = event.getPlayer();
    if (player.getGameMode() != GameMode.SPECTATOR)
    {
      switch (event.getMaterial())
      {
        case FEATHER:
        {
          if (PlayerUtils.isUseItem(event.getAction()))
          {
            event.setUseItemInHand(Event.Result.DENY);
            if (!player.hasCooldown(Material.FEATHER))
            {
              player.setCooldown(Material.FEATHER, 30);
              player.openInventory(new WorldsMenu(player.getWorld(), LocaleUtils.get(player)).getInventory());
            }
          }
          break;
        }
        case COMPASS:
        {
          if (PlayerUtils.isUseItem(event.getAction()))
          {
            event.setUseItemInHand(Event.Result.DENY);
            if (!player.hasCooldown(Material.COMPASS))
            {
              event.setUseInteractedBlock(Event.Result.DENY);
              player.setCooldown(Material.COMPASS, 30);
              player.openInventory(new MyWorldMenu.Worlds(player.getUniqueId().toString(), LocaleUtils.get(player)).getInventory());
            }
          }
          break;
        }
      }
    }
    else
    {
      final User user = UserUtils.get(player.getUniqueId());
      event.setUseInteractedBlock(Event.Result.DENY);
      if (user != null)
        player.openInventory(new KitMenu(user.getHype(), LocaleUtils.get(player)).getInventory());
    }
  }
  
  @Override
  public void onBlockBreak(final BlockBreakEvent event, final Realm realm)
  {
    event.setCancelled(true);
  }
  
  @Override
  public void onBlockPlace(final BlockPlaceEvent event, final Realm realm)
  {
    event.setCancelled(true);
  }
  
  @Override
  public void onPlayerBucketFill(final PlayerBucketFillEvent event, final Realm realm)
  {
    event.setCancelled(true);
  }
  
  @Override
  public void onPlayerBucketEmpty(final PlayerBucketEmptyEvent event, final Realm realm)
  {
    event.setCancelled(true);
  }
  
  @Override
  public void onEntitySpawn(final EntitySpawnEvent event, final Realm realm)
  {
    event.setCancelled(true);
  }
  
  @Override
  public void onEntityDamage(final EntityDamageEvent event, final Realm realm)
  {
    switch (event.getCause())
    {
      case VOID:
      {
        event.setDamage(20.0);
        break;
      }
    }
  }
  
  @Override
  public void onPlayerDeath(final PlayerDeathEvent event, final Realm realm)
  {
    final Player player = event.getEntity();
    player.teleport(player.getWorld().getSpawnLocation());
  }
  
  @Override
  public void onProjectileLaunch(final ProjectileLaunchEvent event, final Realm realm)
  {
  
  }
  
  @Override
  public void onProjectileHit(final ProjectileHitEvent event, final Realm realm)
  {
    event.getEntity().remove();
  }
  
  @Override
  public void onCauldronLevelChange(final CauldronLevelChangeEvent event, final Realm realm)
  {
    event.setCancelled(true);
  }
  
  @Override
  public void onPlayerEditBook(final PlayerEditBookEvent event, final Realm realm)
  {
    event.setCancelled(true);
  }
  
  @Override
  public void onPlayerLocaleChange(final PlayerLocaleChangeEvent event, final Realm realm)
  {
    final Player player = event.getPlayer();
    if (player.getGameMode() == GameMode.SPECTATOR)
      player.sendTitle("", LocaleUtils.get(player).get("title.clickToContinue"), 0, 100, 5);
  }
  
  @Override
  public void unloadWorld(final World world, final Realm realm)
  {
  
  }
  
  @Override
  public void unloadPlayer(final Player player, final World world, final Realm realm)
  {
  
  }
  
  @Override
  public void tickSecond(final World world, final Realm realm)
  {
    final String online = String.valueOf(world.getPlayers().size());
    for (final Player players : world.getPlayers())
      players.sendActionBar(LocaleUtils.get(players).get("actionbar.online").replace("{online}", online));
    world.spawnParticle(Particle.VILLAGER_HAPPY, world.getSpawnLocation(), 8, 0.5, 0.5, 0.5);
  }
  
  @Override
  public String getName()
  {
    return "KitPVP";
  }
  
  @Override
  public Material getIcon()
  {
    return Material.IRON_SWORD;
  }
  
  @Override
  public boolean getPlayerCanStart()
  {
    return true;
  }
  
  @Override
  public boolean getNeedAuth()
  {
    return true;
  }
}