package ru.litegamemc.kitpvp.kits.utils;

import java.util.ArrayList;
import java.util.List;

public class KitUtils
{
  private static final List<Kit> list = new ArrayList<>();
  
  public static Kit get(final String kit)
  {
    for (final Kit kits : list)
    {
      if (kits.getName().equals(kit))
        return kits;
    }
    return list.get(0);
  }
  
  public static List<Kit> list()
  {
    return list;
  }
}